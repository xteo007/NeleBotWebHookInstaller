## NeleBot WebHook Installer

**Repository GitLab ufficiale del WebHook installer per** [NeleBot](https://gitlab.com/PHP-Coders/NeleBotFramework)<br><br>
**Versione WebHook Installer:** <code>2.0</code><br>
**Versione NeleBot:** <code>2.4</code>